//restore cards if any
//new card
//save new card
//show card
//add action to card
//process actions
//save action result

//card data model
//text: some string
//status: 'to-do' | 'in-progress' | 'done'

const saveCards = () => {
  localStorage.setItem('myCard', JSON.stringify(cards));
}
const getCards = () => {
  const savedCardsJson = localStorage.getItem('myCard')
  return JSON.parse(savedCardsJson);
}
//теперь когда страница будет грузиться 
//будем пробовать карточки если ничего нет создается пустой массив
const cards = getCards() || []; 

const input = document.querySelector('input');
const AddNewBtn = document.querySelector('.add-btn');
const toDoPannel = document.querySelector('.to-do.pannel')
const inProgressPannel = document.querySelector('.in-progress.pannel')
const donePannel = document.querySelector('.done.pannel')

const deleteCard = (cardData, cardEl) => {
  const indx = cards.indexOf(cardData);
  cards.splice(indx, 1);
  saveCards();
  cardEl.remove();
}

const moveCard = (cardData, cardEl) => {
  if(cardData.status === 'in-progress') {
    inProgressPannel.appendChild(cardEl);
  } else if (cardData.status === 'done') {
    donePannel.appendChild(cardEl);
  }
}

const changeCardStatus = (cardData, cardEl) => {
  let nextStatus;
  switch(cardData.status){
    case 'to-do': nextStatus = 'in-progress'; break;
    case 'in-progress': nextStatus = 'done'; break;
    default: break; // nextStatus будет undifined
  }
  if(!nextStatus) { //если не определен вызываем функцию удаления
    deleteCard(cardData, cardEl);
  } else {
    cardData.status = nextStatus;
    saveCards();
    moveCard(cardData, cardEl);
  }
}

const createCard = (cardData) => {
  const cardEl = document.createElement('div');
  cardEl.classList.add('card');
  cardEl.innerHTML = `<span>${cardData.text}</span>`
  let targetPannel;
  switch(cardData.status) {
    case 'to-do': targetPannel = toDoPannel; break;
    case 'in-progress': targetPannel = inProgressPannel; break;
    case 'done': targetPannel = donePannel; break;
    default: return;
  }
  targetPannel.appendChild(cardEl)
  const nextActionBtn = document.createElement('button');
  nextActionBtn.innerText = 'Next';
  cardEl.appendChild(nextActionBtn);
  nextActionBtn.addEventListener('click', e => {
    changeCardStatus(cardData, cardEl);
  })
  const deleteBtn = document.createElement('button');
  deleteBtn.innerText = 'X';
  cardEl.appendChild(deleteBtn);
  deleteBtn.addEventListener('click', e => {
    deleteCard(cardData, cardEl);
  });

}

AddNewBtn.addEventListener('click', e => {
  const text = input.value;
  const status = 'to-do';
  const cardData = {
    text, status
  }
  cards.push(cardData);
  saveCards(); //save to ls
  //validation
  createCard(cardData);
})

if(cards.length > 0) {
  for(const card of cards) {
    createCard(card)
  }
}